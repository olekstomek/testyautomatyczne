package speedtest_v3.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import speedtest_v3.pageObject.MainPage;
import speedtest_v3.pageObject.SetUp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class SpeedtestTest extends SetUp {

    private MainPage mainPage;

    @Before
    public void setUp() {
        mainPage = openHomePage();
    }

    @Test
    public void testSpeedTest() throws InterruptedException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        WebDriverWait wait = new WebDriverWait(driver, 70);
        mainPage.operateOnElement(mainPage.getAcceptButton());
        mainPage.createMainRowInFile();

        for (int i = 0; i < mainPage.getNUMBER_OF_TESTS(); i++) {
            TimeUnit.SECONDS.sleep(mainPage.setAndReturnSecondsPauseToStartSpeedTest());
            mainPage.operateOnElement(mainPage.getStartButton());
            Date dateAndTimeNow = new Date();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.result-item:nth-child(2)")));

            mainPage.getResultsFromPage();
            mainPage.getInformationServerServerCityISP();
            mainPage.getInformationDataUnit();
            mainPage.getTestIdentification();
            mainPage.takeScreenshot();
            mainPage.saveResultFileTextToImport(dateAndTimeNow);
            mainPage.returnMaxAndMinDownloadUploadPing();
            mainPage.operateOnElement(mainPage.getIdentityTest());
            mainPage.returnAverageResultDownloadUploadPing();
            mainPage.returnLogsToConsole(simpleDateFormat, i, dateAndTimeNow, mainPage.getDataUnitPing(),
                    mainPage.getDataUnitDownloadAndUpload());

            if (mainPage.getCounterTest() == mainPage.HOW_MANY_TESTS_TO_SERVERS_SEARCHES &&
                    mainPage.getCounterTest() != mainPage.getNUMBER_OF_TESTS()) {
                mainPage.refreshSpeedtestWebsite();
                System.out.println("Search server with the best ping after " +
                        mainPage.HOW_MANY_TESTS_TO_SERVERS_SEARCHES + " test.\n");
            }
        }
    }

    @After
    public void teardown() throws InterruptedException {
        if (driver != null) {
            mainPage.getStatementTest();
            mainPage.saveResultFileText();
            System.out.println("End of work.");
            TimeUnit.SECONDS.sleep(2);
            driver.quit();
        }
    }

}
