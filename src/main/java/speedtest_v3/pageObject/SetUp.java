package speedtest_v3.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import speedtest_v3.temporaryDataProvider.TemporaryDataProvider;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class SetUp {

    public static WebDriver driver;
    private static WebDriverWait wait;

    protected static MainPage openHomePage() {
        //driver = new FirefoxDriver();
        System.setProperty("webdriver.chrome.driver", "C:\\SeleniumDrivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(TemporaryDataProvider.mainUrl);
        assertEquals(driver.getCurrentUrl(), TemporaryDataProvider.expectedUrl);
        wait = new WebDriverWait(driver, 90);

        return new MainPage(driver);
    }
}
