package speedtest_v3.pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

abstract class PageObject {

    protected static WebDriver driver;
    protected static WebDriverWait wait;
    private final static int SECONDS = 120; //TODO max min time WebDriverWait wait = new WebDriverWait(driver, 70);

    /**
     * Construktor for page objects
     * @param driver
     */
    PageObject(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, SECONDS);
    }
}