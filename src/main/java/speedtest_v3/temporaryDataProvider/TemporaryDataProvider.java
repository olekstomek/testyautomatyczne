package speedtest_v3.temporaryDataProvider;

public class TemporaryDataProvider {

    public static String mainUrl = "http://www.speedtest.net/";
    public static String expectedUrl = "http://www.speedtest.net/pl";
    public static String maxDownloadResult = "\nMaximum download result: \n";
    public static String minDownloadResult = "\nMinimum download result: \n";
    public static String maxUploadResult = "\nMaximum upload result: \n";
    public static String minUploadResult = "\nMinimum upload result: \n";
    public static String maxPingResult = "\nMaximum ping result: \n";
    public static String minPingResult = "\nMinimum ping result: \n";
    public static String avgDownloadResult = "\n\nAverage download result: \n";
    public static String avgUploadResult = "\nAverage upload result: \n";
    public static String avgPingResult = "\nAverage ping result: \n";
    public static String statementText = "\nStatement: ";
    public static String dateAndTime = "Date and Time";
    public static String downloadSpeed = "Download speed";
    public static String uploadSpeed = "Upload speed";
    public static String serverCity = "Server city ";
    public static String server = "Server: ";
    public static String internetServiceProvider = "Internet service provider (ISP) \n";
    public static final String speedTestURL = "http://www.speedtest.net";
    public static String currentDate = "\nCurrent Date: ";
    public static String linkToTestNumber = "Link to test number ";
    public static String downloadForTestNumber = "Download for test number ";
    public static String uploadForTestNumber = "Upload for test number ";
    public static String pingForTestNumber = "Ping for test number ";
    public static String savingDataToFile = "Saving data to file ";




}
