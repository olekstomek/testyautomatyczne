import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/*
* Wejśćie na stronę główną, wyszukanie badSearch i sprawdzenie czy strona
* zwraca informacje że nie znaleziono niczego.
* */
public class AutomationPractice1 {
    private WebDriver driver;
    private final String textToInputSearch = "badSearch";
    private final String url = "http://automationpractice.com/index.php";

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.firefoxdriver().setup();
    }

    @Before
    public void setupTest() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }

    @After
    public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    public void testSearchWithoutResult() {
        driver.get(url);
        WebElement searchInput = driver.findElement(By.id("search_query_top"));

        searchInput.clear();
        searchInput.sendKeys(textToInputSearch);
        searchInput.sendKeys(Keys.ENTER);

        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.urlToBe("http://automationpractice.com/index.php?controller=search&orderby=position&orderway=desc&search_query=badSearch&submit_search="));

        Assert.assertTrue(driver.getPageSource().contains("No results were found for your search"));

        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
