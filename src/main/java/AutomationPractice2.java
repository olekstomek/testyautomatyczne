import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class AutomationPractice2 {
    private WebDriver driver;
    private Actions action;
    private final String url = "http://automationpractice.com/index.php";
    
/*    Consumer<By> hover = (By by) -> {
        action.moveToElement(driver.findElement(by))
              .perform();
    };*/

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.firefoxdriver().setup();
    }

    @Before
    public void setupTest() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        action = new Actions(driver);
    }

    @After
    public void teardown() {
/*        if (driver != null) {
            driver.quit();
        }*/
    }

    @Test
    public void testTryBuyBlouse() {
        driver.get(url);

        Actions action = new Actions(driver);
        WebElement webElementWomenInMenu = driver.findElement(By.linkText("Women"));

        WebDriverWait wait = new WebDriverWait(driver, 3);
        action.moveToElement(webElementWomenInMenu)
                //.build()
                .perform();
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#block_top_menu > ul > li:nth-child(1) > ul > li:nth-child(1) > ul > li:nth-child(2) > a")));
        driver.findElement(By.linkText("Blouses")).click();

        Assert.assertTrue(driver.getTitle().contains("Blouses - My Store"));
        Assert.assertTrue(driver.getCurrentUrl().contains("http://automationpractice.com/index.php?id_category=7&controller=category"));
        Assert.assertTrue(driver.getPageSource().contains("content_scene_cat_bg"));
        Assert.assertFalse(driver.getTitle().contains("T-shirts - My Store"));
        
/*        hover.accept(By.linkText("Women"));
        hover.accept(By.linkText("Blouses"));*/

    }

}
