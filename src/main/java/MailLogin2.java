import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class MailLogin2 {
    private WebDriver driver;
    private String url;
    private String userName;
    private String userPass;
    
    @BeforeClass
    public static void setupClass() {
        WebDriverManager.firefoxdriver().setup();
    }
    
    @Before
    public void setupTest() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        url = "http://www.wp.pl";
        userName = "user@wp.pl";
        userPass = "badPass";
    }
    
    @After
    public void teardown() {
        if (driver != null) {
           // driver.quit();
        }
    }
    
    @Test
    public void testLoginNegative() throws InterruptedException
    {
        driver.get(url);
        driver.findElement(By.xpath("/html/body/div[3]/div/div/div[3]/div[1]")).click();
        TimeUnit.SECONDS.sleep(3);
        driver.findElement(By.partialLinkText("Poczta")).click();
        
        WebElement loginName = driver.findElement(By.name("login_username"));
        loginName.clear();
        loginName.sendKeys(userName);
        
        WebElement loginPass = driver.findElement(By.id("password"));
        loginPass.clear();
        loginPass.sendKeys(userPass);
        
        driver.findElement(By.id("btnSubmit")).click();
        
        Assert.assertTrue(driver.getPageSource().contains("Niestety podany login lub hasło jest błędne."));
        
        TimeUnit.SECONDS.sleep(3);
    }
    
}